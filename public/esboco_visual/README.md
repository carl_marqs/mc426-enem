# Esboço da parte HTML do projeto
 O principal objetivo foi montar um site minimalista e agradável ao usuário, usando as cores predominantes do logo do enem e que fosse fácil de ser usado.  
 Por isso optamos por uma tela inicial mais clean e com poucos botões/informações que poderiam tirar o foco principal d site:

![](esboco_site.png)<br/>

 Na tela inicial teremos 3 botões de destaque: Análise de Dados, Começar o Teste e Quem Somos.  
 Análise de Dados:
 
 ![](esboco_analise.png)<br/>
 
 Aqui exibiremos as análises que fizemos previamente com os dados do ENEM 2019, onde o usuário desce pela barra de rolagem para visualizar todos os gráficos  
 
 Começar o Teste:
 
 ![](esboco_teste.png)<br/>
 
 ![](esboco_teste_final.png)<br/>
 
 Aqui será onde o usuário colocará os inputs com suas informações para que o site possa rodar um algorítmo de análise dos dados e tentar predizer qual será a nota deleno próximo enem

Queem Somos:

![](esboco_quem_somos.png)<br/>

Um breve texto de quem somos e motivo do projeto. Informações de contato serão fornecidas se por acaso algum erro for achado ou se quiserem no enviar algo.