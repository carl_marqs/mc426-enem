## Build e Setup
Para instalar as dependências do projeto, primeiramente, na parte em python (Back-End), usamos um pip3 install para instalar as dependências pandas, numpy, seaborn, tensorflow  e kera no arquivo enem-analyse/setup
Já para usar este resultado em nosso módulo visual HTML, usamos o TensorFlow, que consegue usar o arquivo de treino da rede neural para conseguirmos um resultado para a resposta do questionário.

## Testes
Para executar os testes, instalar a extensão do Selenium no Browser e executá-las no site.