async function predict(answers) {
  document.getElementById("log").textContent = "Calculando predição...";
  console.log("[predict] DEBUG: answers: " + answers);
  var data = answers.slice();
  for (var i = 8; i < data.length; i++) {
    if ([3,4,8,9,10,11,12,13,14,15,16,17,19,22,24].includes(i-7)) {

      data[i] = (data[i]/4);
    }
    else if ([1,2].includes(i-7))
    {
      data[i] = (data[i]/6);
    }
    else if (i-7 == 5)
    {
      data[i] = (data[i]/19);
    }
    else if (i-7 == 6)
    {
      data[i] = (data[i]/16);
    }
    else if (i-7 == 7)
    {
      data[i] = (data[i]/3);
    }
  }

  var uf = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  uf[answers[3] + 1] = 1;
  var civil_status = [0,0,0,0,0];
  civil_status[answers[4] + 1] = 1;
  var color = [0,0,0,0,0,0,0];
  color[answers[5] + 1] = 1;
  var conclusion = [0,0,0,0,0,0,0,0,0,0,0,0,0];
  conclusion[answers[6] + 1] = 1;
  var school = [0,0,0,0,0];
  school[answers[7] - 1] = 1;

  data = [].concat(data.slice(0,3), data.slice(8), uf, civil_status, color, conclusion, school);
  console.log("[predict] DEBUG: data: " + data);

  const model = await tf.loadLayersModel('https://carl.marqs.gitlab.io/mc426-enem/model.json');
  const prediction = Array.from(model.predict(tf.tensor(data).reshape([1, 86])).arraySync())[0];
  console.log("[predict] MESSSAGE: Prediction result: " + prediction);
  document.getElementById("log").innerHTML = "Sua nota esperada do Enem é:<br><br>Matemática: " + Math.round(prediction[0]) + "<br>Ciências da Natureza: " + Math.round(prediction[1]) + "<br>Ciências Humanas: " + Math.round(prediction[2]) + "<br>Linguagens e Códigos: " + Math.round(prediction[3]) + "<br>Redação: " + Math.round(prediction[4]);
}

function validateSurveyForm() {
  document.getElementById("log").textContent = "Verificando respostas...";

  var answers = [(parseFloat(document.querySelectorAll('input[name="age"]')[0].value) - 10) / 90];
  console.log("[validateSurveyForm] DEBUG: Question 'age' value: " + answers[0]);

  var questions = ["gender", "language", "uf", "civil_status", "color", "conclusion", "school"];
  for (var i = 0; i < questions.length; i++) {
    if (i == 2 || i == 5) {
      var element = document.querySelector('select[name="'+ questions[i] + '"]');
      answers.push(parseFloat(element.value));
      console.log("[validateSurveyForm] DEBUG: Question '" + questions[i] + "' value: " + parseFloat(element.value));
      continue;
    };
    var checked = document.querySelectorAll('input[name="'+ questions[i] + '"]:checked');
    if (checked.length < 1 || checked.length > 1) {
      //window.alert("ERRO: Questão " + i + " em branco ou inválida!");
      console.log("[validateSurveyForm] ERROR: Question '" + questions[i] + "' blank or invalid!");
      document.getElementById("log").textContent = "ERRO: Questão '" + questions[i] + "' em branco ou inválida!";
      return;
    }
    else {
      console.log("[validateSurveyForm] DEBUG: Question '" + questions[i] + "' value: " + parseFloat(checked[0].value));
      answers.push(parseFloat(checked[0].value));
    }
  }

  for (var i = 1; i <= 25; i++) {
    if (i == 5) {
      var element = document.querySelector('select[name="question_'+ i + '"]');
      answers.push(parseFloat(element.value));
      console.log("[validateSurveyForm] DEBUG: Question " + i + " value: " + parseFloat(element.value));
      continue;
    };
    var checked = document.querySelectorAll('input[name="question_'+ i + '"]:checked');
    if (checked.length < 1 || checked.length > 1) {
      //window.alert("ERRO: Questão " + i + " em branco ou inválida!");
      console.log("[validateSurveyForm] ERROR: Question " + i + " blank or invalid!");
      document.getElementById("log").textContent = "ERRO: Questão " + i + " em branco ou inválida!";
      return;
    }
    else {
      console.log("[validateSurveyForm] DEBUG: Question " + i + " value: " + parseFloat(checked[0].value));
      answers.push(parseFloat(checked[0].value));
    }
  }

  document.getElementById("log").textContent = "Respostas válidas!";
  console.log("[validateSurveyForm] MESSSAGE: Survey form is valid!");
  predict(answers);
}

function toggleContent(selected_id)
{
  // Hide other contents
  if (selected_id != 'analysis') {
    document.getElementById('analysis').style.display = 'none';
    document.getElementById('btn_analysis').style.borderBottomColor = 'white';
  }
  if (selected_id != 'survey') {
    document.getElementById('survey').style.display = 'none';
    document.getElementById('btn_survey').style.borderBottomColor = 'white';
  }
  if (selected_id != 'who') {
    document.getElementById('who').style.display = 'none';
    document.getElementById('btn_who').style.borderBottomColor = 'white';
  }

  // Toggle selected
  if (document.getElementById(selected_id).style.display == 'none') {
    document.getElementById(selected_id).style.display = 'inherit';
    document.getElementById('btn_' + selected_id).style.borderBottomColor = '#ED1C24';
  }
}

function updateTextInput(val) {
  document.getElementById('ageTxtInput').value = val;
}

function updateRangeInput(val) {
  if (val >= 10 && val <= 100)
  {
    document.getElementById('ageRangeInput').value = val;
  }
}
